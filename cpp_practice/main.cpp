//
//  main.cpp
//  
//
//  Created by Brian Summa on 8/21/18.
//

#include <iostream>

//http://www.cplusplus.com/reference/vector/vector/
//http://www.cplusplus.com/reference/cstdio/
//http://www.cplusplus.com/reference/iostream/
//http://www.cplusplus.com/reference/cstring/

using namespace std;

int main()
{
  cout << "Hello World";
  
  //1: Create a vector of 32 unsigned 8-bit integers with values [0-31].
  
  //2: Create an array of 32 single precision floats with values [0.0-31.0].
  
  //3: Create an array of 32 vec3 (class from vec.h) with values [(1,2,3),(4,5,6),...]

  //4: Iterate over previous array as a memory offset, not by index.

  //5: normalize each element in the array.

  //6: Copy the 3 array into a new array of 32 vec3s using memcpy

  
  return 0;
}
